package com.galaxiatechsolutions.jobschedular.jobschedularosproject;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "EXAMPLE Main Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void scheduleJob(View v)
    {
        ComponentName componentName = new ComponentName(this, classJobService.class);
        JobInfo info = new JobInfo.Builder(123, componentName)
                .setRequiresCharging(true) // When Device is in charging state
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED) //WIFI
                .setPersisted(true) //boolean: True to indicate that the job will be written to disk and loaded at boot.
                .setMinimumLatency(1000) //long: Milliseconds before which this job will not be considered for execution.
                .build();

        JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        int resultCode = scheduler.schedule(info);

        if (resultCode == JobScheduler.RESULT_SUCCESS)
        {
            Log.d(TAG, "JOB SCHEDULED!");
        }
        else {
            Log.d(TAG, "JOB SCHEDULING FAILED!");
        }


    }

    public void cancelJob(View v)
    {
        JobScheduler scheduler = (JobScheduler) getSystemService(JOB_SCHEDULER_SERVICE);
        scheduler.cancel(123);
        Log.d(TAG,"JOB CANCELLED!");
    }
}

// THIS CLASS DEFINES THAT UNDER WHAT CIRCUMSTANCES WE WANT TO START OUR BACKGROUND SERVICES.