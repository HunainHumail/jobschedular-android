package com.galaxiatechsolutions.jobschedular.jobschedularosproject;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;

public class classJobService extends JobService {
    private static final String TAG = "Example JOB SERVICE";
    private boolean jobCancelled = false;
    @Override
    public boolean onStartJob(JobParameters params) {
        Log.d(TAG, "JOB STARTED!"); //when this method is excuted then it will print JOB START in terminal
        doBackgroundWork(params); //we pass params form JobParameters

        //in most cases you wanted to run some long running background operations like connecting to a server this job server is by default runs in the UI thread
        return true; // false tells that this method is over when our job is over
        //we return true when we run long background operations
    }

    private void doBackgroundWork(final JobParameters params)

            //Making Fake Thread (we will start a thread and then freeze it for couple of seconds)
    {
        new Thread(new Runnable() {
            @Override
            public void run() {

                for (int i = 0; i < 10 ; i++)
                {
                    Log.d(TAG, "run: " + i);
                    if (jobCancelled)
                    {
                        return; //if job is cancelled then we will leave this method and stop executing background work.
                    }

                    try {
                        Thread.sleep(1000); // freezing for 1 second after every iteration
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                Log.d(TAG, "JOB FINISHED!");
                jobFinished(params, false); //if something fails then Reschedule is necessary, we don not need reschedule so we have make it false

            }
        }).start();

    }

    @Override
    public boolean onStopJob(JobParameters params) { //WILL BE CALLED WHEN OUR JOB IS CANCELLED!
        Log.d(TAG, "JOB CANCELLED BEFORE COMPLETION"); // When job is cancelled then it means that direct log is not kept for our app anymore

        //but we are responsible for stopping our background work ourselves, if we dont do this then our application will misbehave
        //jobCancelled=false; X thats why we created this boolean jobCancelled
        jobCancelled=true;
        return true;
        //if we are using asynctask for background work then we will use asynctask.cancelled


        // return false; // if our work is not important than we can return false, if we want to reschedule the job because our work is important then return true

    }
}
